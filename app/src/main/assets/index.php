<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Mechmech Municipality</title>

    <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Theme CSS -->
    <link href="css/agency.min.css" rel="stylesheet">
    <link href="css/scrollbar.css" rel="stylesheet">
    <link href="css/app.css" rel="stylesheet">
</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand page-scroll" href="#page-top">Municipality of Mechmech</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#services">طلبات ومعاملات</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#portfolio">مؤسسات وجمعيات</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#about">المزيد</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#contact">اتصل بنا</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="#announecements">اعلانات</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="login.php">Admin Login</a>
                    </li>
					<!--<li>
						<a href="app_en.html">Eng</a>
					</li>-->
                </ul>
            </div>
        </div>
    </nav>

    <!-- Header -->
    <header>
        <div class="container">
            <div class="intro-text">
                <div data-intro="This is the name of this site" class="intro-lead-in">Welcome To Mechmech</div>
                <div  data-intro="This is the main headline" class="intro-heading"></div>
                <a href="#services" class="btn btn-xl page-scroll">اكتشف</a>
            </div>
        </div>
    </header>

    <!-- Services Section -->
    <section id="services">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">طلبات ومعاملات</h2>
                    <h3 class="section-subheading text-muted">طلبات ومعاملات وانجازات</h3>
                </div>
            </div>
            <div class="row text-center">
                <div class="col-md-4">
					<a href="#portfolioModal7" class="portfolio-link" data-toggle="modal" style="text-decoration:none; color:#333">
						<span class="fa-stack fa-4x">
							<i class="fa fa-circle fa-stack-2x text-primary"></i>
							<i class="fa fa-book fa-stack-1x fa-inverse"></i>
						</span>
						<h4 class="service-heading">أوراق ومعاملات</h4>
                    </a>
					<p class="text-muted">أوراق ومعاملات</p>
                </div>
                <div class="col-md-4">
					<a href="#portfolioModal8" class="portfolio-link" data-toggle="modal" style="text-decoration:none; color:#333">
						<span class="fa-stack fa-4x">
							<i class="fa fa-circle fa-stack-2x text-primary"></i>
							<i class="fa fa-laptop fa-stack-1x fa-inverse"></i>
						</span>
						<h4 class="service-heading">الهيئة الإدارية</h4>
					</a>	
                    <p class="text-muted">الهيئة الإدارية منذ تأسيسها</p>
                </div>
                <div class="col-md-4">
					<a href="#portfolioModal9" class="portfolio-link" data-toggle="modal" style="text-decoration:none; color:#333">
						<span class="fa-stack fa-4x">
							<i class="fa fa-circle fa-stack-2x text-primary"></i>
							<i class="fa fa-lock fa-stack-1x fa-inverse"></i>
						</span>
						<h4 class="service-heading">مشاريع وانجازات</h4>
					</a>
                    <p class="text-muted">مشاريع وانجازات</p>
                </div>
            </div>
        </div>
    </section>
	
	<?php
		$file = fopen("announcements.txt", "r");
		while(!feof($file)) {
			$line = fgets($file);
			$parts = explode(',', $line);
		}
		fclose($file);
	?>
	
	<!-- Announcements Section -->
    <section id="announecements">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">اعلانات</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-primary">
                    <ul>
						<li><?php echo $parts[1]; ?></li>
					</ul>
                    <ul>
						<li>hjfngrjk ggbejk gbrgj rgbjkr gkj grhgk kg g</li>
					</ul>
                    <ul>
						<li>hjfngrjk ggbejk gbrgj rgbjkr gkj grhgk kg g</li>
					</ul>
                </div>
            </div>
        </div>
    </section>

    <!-- Portfolio Grid Section -->
    <section id="portfolio" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">مؤسسات وجمعيات</h2>
                    <h3 class="section-subheading text-muted">مؤسسات وجمعيات ترفيهية في مشمش</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal1" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/school/main.jpg" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>مدرسة مشمش</h4>
                        <p class="text-muted">مدرسة رسمية</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal2" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/clinic/main.jpg" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>المستوصف</h4>
                        <p class="text-muted">المستوصف الخيري</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal3" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/club/main.jpg" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>النادي الرياضي</h4>
                        <p class="text-muted">نادي مشمش الرياضي</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal4" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/churches/main.jpg" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>كنائس</h4>
                        <p class="text-muted">كنائس وأوقاف</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal5" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/chabibe/main.jpg" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>شبيبة وطلائع</h4>
                        <p class="text-muted">شبيبة وطلائع</p>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 portfolio-item">
                    <a href="#portfolioModal6" class="portfolio-link" data-toggle="modal">
                        <div class="portfolio-hover">
                            <div class="portfolio-hover-content">
                                <i class="fa fa-plus fa-3x"></i>
                            </div>
                        </div>
                        <img src="img/retail/main.png" class="img-responsive" alt="">
                    </a>
                    <div class="portfolio-caption">
                        <h4>محلات تجارية</h4>
                        <p class="text-muted">محلات تجارية</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- About Section -->
    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">المزيد</h2>
                    <h3 class="section-subheading text-muted">الجدول الزمني للبلدية منذ تأسيسها</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <ul class="timeline">
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/timeline/1.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>حزيران  2004</h4>
                                    <h4 class="subheading">تأسيس البلدية</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/timeline/2.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>حزيران 2010</h4>
                                    <h4 class="subheading">المرحلة الثانية</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                </div>
                            </div>
                        </li>
                        <li class="timeline-inverted">
                            <div class="timeline-image">
                                <img class="img-circle img-responsive" src="img/timeline/3.jpg" alt="">
                            </div>
                            <div class="timeline-panel">
                                <div class="timeline-heading">
                                    <h4>أيار 2016</h4>
                                    <h4 class="subheading">الحالية</h4>
                                </div>
                                <div class="timeline-body">
                                    <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt ut voluptatum eius sapiente, totam reiciendis temporibus qui quibusdam, recusandae sit vero unde, sed, incidunt et ea quo dolore laudantium consectetur!</p>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
	
	<!-- Clients Aside -->
    <aside class="clients">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6">
                    <a href="#">
                        <img src="img/ads/designmodo.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-4 col-sm-6">
                    <a href="#">
                        <img src="img/ads/themeforest.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
                <div class="col-md-4 col-sm-6">
                    <a href="#">
                        <img src="img/ads/creative-market.jpg" class="img-responsive img-centered" alt="">
                    </a>
                </div>
            </div>
        </div>
    </aside>

    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading">اتصل بنا</h2>
                    <h3 class="section-subheading text-muted">Mechmech, Jbeil, Mount Lebanon, Lebanon.</h3>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 text-primary">
                    <ul>
						<li>Tel: 09760400 | 09760401</li>
					</ul>
                    <ul>
						<li>Email: info@mechmech.org</li>
					</ul>
                    <ul>
						<li>Fax: 09760400</li>
					</ul>
                </div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <span class="copyright">Copyright &copy; mechmech.org 2018</span>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="https://www.facebook.com/groups/2549753991/" target="blank"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="javascript:void(0)"><i class="fa fa-instagram"></i></a>
                        </li>
                        <li><a href="javascript:void(0)"><i class="fa fa-whatsapp"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="javascript:void(0)"><b>Designed and developed by Roudy Khoury</b></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>

    <!-- Portfolio Modals -->

    <!-- Portfolio Modal 1 -->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2>المدرسة</h2>
                                <div id="carousel1" class="carousel slide" data-ride="carousel">
									<!-- Menu -->
									<ol class="carousel-indicators">
										<li data-target="#carousel1" data-slide-to="0" class="active"></li>
										<li data-target="#carousel1" data-slide-to="1"></li>
										<li data-target="#carousel1" data-slide-to="2"></li>
									</ol>
									
									<!-- Items -->
									<div class="carousel-inner">
										<div class="item active">
											<img src="img/school/main.jpg" alt="Slide 0" />
										</div>
										<div class="item">
											<img src="img/school/2.jpg" alt="Slide 1" />
										</div>
										<div class="item">
											<img src="img/school/3.jpg" alt="Slide 2" />
										</div>
									</div> 
									<a href="#carousel1" class="left carousel-control" data-slide="prev">
										<span class="fa fa-arrow-left glyphicon glyphicon-chevron-left"></span>
									</a>
									<a href="#carousel1" class="right carousel-control" data-slide="next">
										<span class="fa fa-arrow-right glyphicon glyphicon-chevron-right"></span>
									</a>
								</div>
								<p class="item-intro text-muted">مدرسة مشمش الرسمية</p>
                                <ul class="list-inline">
                                    <li>Tel: 09760500</li>
                                    <li>Date: 1905</li>
                                    <li>التلاميذ: 60</li>
                                    <li>المدير: مينرفا الخوري حنا</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Portfolio Modal 2 -->
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <h2>المستوصف</h2>
                                <div id="carousel2" class="carousel slide" data-ride="carousel">
									<!-- Menu -->
									<ol class="carousel-indicators">
										<li data-target="#carousel2" data-slide-to="0" class="active"></li>
									</ol>
									
									<!-- Items -->
									<div class="carousel-inner">
										<div class="item active">
											<img src="img/clinic/main.jpg" alt="Slide 0" />
										</div>
									</div> 
									<a href="#carousel2" class="left carousel-control" data-slide="prev">
										<span class="fa fa-arrow-left glyphicon glyphicon-chevron-left"></span>
									</a>
									<a href="#carousel2" class="right carousel-control" data-slide="next">
										<span class="fa fa-arrow-right glyphicon glyphicon-chevron-right"></span>
									</a>
								</div>
								<p class="item-intro text-muted">مستوصف مشمش الخيري</p>
								<ul class="list-inline">
                                    <li>Tel: 09760500</li>
                                    <li>Date: 1905</li>
                                    <li>الرئيس: جوزاف حنا</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Portfolio Modal 3 -->
    <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2>النادي الرياضي</h2>
                                <div id="carousel3" class="carousel slide" data-ride="carousel">
									<!-- Menu -->
									<ol class="carousel-indicators">
										<li data-target="#carousel3" data-slide-to="0" class="active"></li>
									</ol>
									
									<!-- Items -->
									<div class="carousel-inner">
										<div class="item active">
											<img src="img/club/main.jpg" alt="Slide 0" />
										</div>
									</div> 
									<a href="#carousel3" class="left carousel-control" data-slide="prev">
										<span class="fa fa-arrow-left glyphicon glyphicon-chevron-left"></span>
									</a>
									<a href="#carousel3" class="right carousel-control" data-slide="next">
										<span class="fa fa-arrow-right glyphicon glyphicon-chevron-right"></span>
									</a>
								</div>
								<p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Portfolio Modal 4 -->
    <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2>كنائس</h2>
								<div id="carousel4" class="carousel slide" data-ride="carousel">
									<!-- Menu -->
									<ol class="carousel-indicators">
										<li data-target="#carousel4" data-slide-to="0" class="active"></li>
										<li data-target="#carousel4" data-slide-to="1"></li>
										<li data-target="#carousel4" data-slide-to="2"></li>
										<li data-target="#carousel4" data-slide-to="3"></li>
										<li data-target="#carousel4" data-slide-to="4"></li>
										<li data-target="#carousel4" data-slide-to="5"></li>
									</ol>
									
									<!-- Items -->
									<div class="carousel-inner">
										<div class="item active">
											<img src="img/churches/main.jpg" alt="Slide 0" />
										</div>
										<div class="item">
											<img src="img/churches/2.jpg" alt="Slide 1" />
										</div>
										<div class="item">
											<img src="img/churches/3.jpg" alt="Slide 2" />
										</div>
										<div class="item">
											<img src="img/churches/4.jpg" alt="Slide 3" />
										</div>
										<div class="item">
											<img src="img/churches/5.jpg" alt="Slide 4" />
										</div>
										<div class="item">
											<img src="img/churches/6.jpg" alt="Slide 5" />
										</div>
									</div> 
									<a href="#carousel4" class="left carousel-control" data-slide="prev">
										<span class="fa fa-arrow-left glyphicon glyphicon-chevron-left"></span>
									</a>
									<a href="#carousel4" class="right carousel-control" data-slide="next">
										<span class="fa fa-arrow-right glyphicon glyphicon-chevron-right"></span>
									</a>
								</div>
								<p class="item-intro text-muted">كنائس</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Portfolio Modal 5 -->
    <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2>شبيبة وطلائع</h2>
								<div id="carousel5" class="carousel slide" data-ride="carousel">
									<!-- Menu -->
									<ol class="carousel-indicators">
										<li data-target="#carousel5" data-slide-to="0" class="active"></li>
										<li data-target="#carousel5" data-slide-to="1"></li>
										<li data-target="#carousel5" data-slide-to="2"></li>
										<li data-target="#carousel5" data-slide-to="3"></li>
									</ol>
									
									<!-- Items -->
									<div class="carousel-inner">
										<div class="item active">
											<img src="img/chabibe/main.jpg" alt="Slide 0" />
										</div>
										<div class="item">
											<img src="img/chabibe/2.jpg" alt="Slide 1" />
										</div>
										<div class="item">
											<img src="img/chabibe/3.jpg" alt="Slide 2" />
										</div>
										<div class="item">
											<img src="img/chabibe/4.jpg" alt="Slide 3" />
										</div>
									</div> 
									<a href="#carousel5" class="left carousel-control" data-slide="prev">
										<span class="fa fa-arrow-left glyphicon glyphicon-chevron-left"></span>
									</a>
									<a href="#carousel5" class="right carousel-control" data-slide="next">
										<span class="fa fa-arrow-right glyphicon glyphicon-chevron-right"></span>
									</a>
								</div>
                                <p class="item-intro text-muted">شبيبة وطلائع</p>
								<a href="www.instgram.com/chabibet_mechmech/"><i class="fa fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Portfolio Modal 6 -->
    <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2>محلات تجارية</h2>
                                <div id="carousel6" class="carousel slide" data-ride="carousel">
									<!-- Menu -->
									<ol class="carousel-indicators">
										<li data-target="#carousel6" data-slide-to="0" class="active"></li>
									</ol>
									
									<!-- Items -->
									<div class="carousel-inner">
										<div class="item active">
											<img src="img/retail/main.png" alt="Slide 0" />
										</div>
									</div> 
									<a href="#carousel6" class="left carousel-control" data-slide="prev">
										<span class="fa fa-arrow-left glyphicon glyphicon-chevron-left"></span>
									</a>
									<a href="#carousel6" class="right carousel-control" data-slide="next">
										<span class="fa fa-arrow-right glyphicon glyphicon-chevron-right"></span>
									</a>
								</div>
								<p class="item-intro text-muted">محلات تجارية</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<!-- Portfolio Modal 7 -->
    <div class="portfolio-modal modal fade" id="portfolioModal7" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2>أوراق وطلابات</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <p>Dreams is a free PSD web template built by <a href="https://www.behance.net/MathavanJaya">Mathavan Jaya</a>. Dreams is a modern one page web template designed for almost any purpose. It’s a beautiful template that’s designed with the Bootstrap framework in mind.</p>
                                <p>You can download the PSD template in this portfolio sample item at <a href="http://freebiesxpress.com/gallery/dreams-free-one-page-web-template/">FreebiesXpress.com</a>.</p>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<!-- Portfolio Modal 8 -->
    <div class="portfolio-modal modal fade" id="portfolioModal8" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2>الهيئة الإدارية</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <p>Dreams is a free PSD web template built by <a href="https://www.behance.net/MathavanJaya">Mathavan Jaya</a>. Dreams is a modern one page web template designed for almost any purpose. It’s a beautiful template that’s designed with the Bootstrap framework in mind.</p>
                                <p>You can download the PSD template in this portfolio sample item at <a href="http://freebiesxpress.com/gallery/dreams-free-one-page-web-template/">FreebiesXpress.com</a>.</p>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
	
	<!-- Portfolio Modal 9 -->
    <div class="portfolio-modal modal fade" id="portfolioModal9" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-dismiss="modal">
                    <div class="lr">
                        <div class="rl">
                        </div>
                    </div>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-lg-offset-2">
                            <div class="modal-body">
                                <!-- Project Details Go Here -->
                                <h2>مشاريع وانجازات</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <p>Dreams is a free PSD web template built by <a href="https://www.behance.net/MathavanJaya">Mathavan Jaya</a>. Dreams is a modern one page web template designed for almost any purpose. It’s a beautiful template that’s designed with the Bootstrap framework in mind.</p>
                                <p>You can download the PSD template in this portfolio sample item at <a href="http://freebiesxpress.com/gallery/dreams-free-one-page-web-template/">FreebiesXpress.com</a>.</p>
                                <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close Project</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendor/jquery/jquery.easing.min.js"></script>

    <!-- Theme JavaScript -->
    <script src="js/agency.min.js"></script>

</body>
</html>